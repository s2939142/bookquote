package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String, Double> bookPrice;
    public Quoter(){
        bookPrice = new HashMap<>();
        bookPrice.put("1", 10.0);
        bookPrice.put("2", 45.0);
        bookPrice.put("3", 20.0);
        bookPrice.put("4", 35.0);
        bookPrice.put("5", 50.0);
    }
    public double getBookPrice(String isbn){
        if (bookPrice.containsKey(isbn))
            return bookPrice.get(isbn);
        return 0.0;
    }
}
